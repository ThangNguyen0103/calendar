package huntermacdonald.com.calendar.interfaces;

/**
 * Created by quoctran on 27/10/2015.
 */
public interface RequestCallbackDb<T> {
    void onDbResult(T data);
}
