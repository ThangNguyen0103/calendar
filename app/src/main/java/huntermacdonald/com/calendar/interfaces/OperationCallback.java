package huntermacdonald.com.calendar.interfaces;

/**
 * Created by quoctran on 01/12/2015.
 */
public interface OperationCallback<T> {

    void onSuccess(T result);

    void onError(Exception e);
}
