package huntermacdonald.com.calendar.interfaces;

/**
 * Created by quoctran on 02/12/2015.
 */
public interface RequestCallback<T>{

    void onSuccess(T data);
    void onError();
}
