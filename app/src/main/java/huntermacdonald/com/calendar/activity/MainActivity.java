package huntermacdonald.com.calendar.activity;

import android.Manifest;
import android.accounts.Account;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;

import android.provider.CalendarContract;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import com.alamkanak.weekview.WeekViewEvent;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.util.ExponentialBackOff;

import com.google.api.services.calendar.CalendarScopes;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.microsoft.aad.adal.AuthenticationCallback;
import com.microsoft.aad.adal.AuthenticationResult;
import com.microsoft.aad.adal.AuthenticationSettings;
import com.microsoft.outlookservices.Event;
import com.microsoft.outlookservices.odata.OutlookClient;
import com.microsoft.services.odata.impl.ADALDependencyResolver;

import android.accounts.AccountManager;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.net.URI;
import java.util.*;
import java.util.Calendar;

import huntermacdonald.com.calendar.R;
import huntermacdonald.com.calendar.SessionManager;
import huntermacdonald.com.calendar.WebServiceHandler;
import huntermacdonald.com.calendar.adapter.ListAccountAdapter;
import huntermacdonald.com.calendar.database.CalendarDatabaseHelper;
import huntermacdonald.com.calendar.database.DatabaseHandler;
import huntermacdonald.com.calendar.fragment.CalendarMonthlyFragment;
import huntermacdonald.com.calendar.fragment.CalendarViewWeekly;
import huntermacdonald.com.calendar.fragment.DetailCalendarFragment;
import huntermacdonald.com.calendar.interfaces.RequestCallback;
import huntermacdonald.com.calendar.interfaces.RequestCallbackDb;
import huntermacdonald.com.calendar.model.AccountCalendar;
import huntermacdonald.com.calendar.model.DataCalendar;
import huntermacdonald.com.calendar.outlook.AuthenticationManager;
import huntermacdonald.com.calendar.utils.Statics;
import huntermacdonald.com.calendar.utils.Util;
import yuku.ambilwarna.AmbilWarnaDialog;

public class MainActivity extends AppCompatActivity implements CalendarMonthlyFragment.OnDateSelect, CalendarViewWeekly.OnDateSelectInWeekly, ListAccountAdapter.OnListenerAccountSelect {

    private static final String LOG_TAG = "MainActivity";


    GoogleAccountCredential mCredential;
    private static final String[] SCOPES = {CalendarScopes.CALENDAR_READONLY};

    protected WebServiceHandler mWebServiceHandler;
    protected SessionManager sessionManager;

    private Button btnChangeAccount;
    private DrawerLayout mDrawerLayout;
    private Toolbar toolbar;
    private RecyclerView rvAccount;
    private ProgressBar progressSync;

    private DetailCalendarFragment mDetailCalendarFragment;
    private CalendarMonthlyFragment mCalendarMonthlyFragment;
    private CalendarViewWeekly mCalendarViewWeekly;
    private ListAccountAdapter mListAccountAdapter;
    private DatabaseHandler mDatabaseHandler;
    private CalendarDatabaseHelper mDb;
    private List<AccountCalendar> listAccount = new ArrayList<>();
    private String accountOutlook;

    //banner show ads Admob
    private AdView mAdView;
    private AdRequest adRequest;


    private BroadcastReceiver mBroadcastReceiverEventUpdate = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (Statics.ACTION_NEW_EVENT.equals(intent.getAction())) {
                checkPermissionReadCalendar();
            }
        }
    };

    private BroadcastReceiver mBroadcastReceiverNetWorkChange = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            showBanner(Util.isNetworkAvailable());
        }


    };

    /**
     * Create the main activity.
     *
     * @param savedInstanceState previously saved instance data.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_main);

        sessionManager = new SessionManager(getApplicationContext());


        if (Integer.valueOf(Build.VERSION.SDK_INT) > 22) {
            insertDummyContactWrapper(Statics.REQUEST_CODE_ASK_PERMISSIONS_ALL);
        }

        mWebServiceHandler = new WebServiceHandler(getApplicationContext());

        mapView();

        initData();

        setEventListener();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Statics.ACTION_NEW_EVENT);
        registerReceiver(mBroadcastReceiverEventUpdate, intentFilter);
        IntentFilter intentFilterNetWorkChange = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(mBroadcastReceiverNetWorkChange, intentFilterNetWorkChange);

    }

    private void getListAccountGoogle() {
        AccountManager accountManager = AccountManager.get(MainActivity.this);
        Account[] accounts = accountManager.getAccountsByType(getResources().getString(R.string.main_account_type_google));
        for (int i = 0; i < accounts.length; i++) {
            AccountCalendar accountCalendar = new AccountCalendar();
            accountCalendar.accountName = accounts[i].name;
            accountCalendar.isType = true;
            accountCalendar.isCheck = sessionManager.getCheckBox(accounts[i].name);
            accountCalendar.isSync = false;
            listAccount.add(accountCalendar);
        }
        listAccount.addAll(mDb.getAccountOutlook());
    }


    private void insertDummyContactWrapper(int typeRequest) {
        int statusPermissionAccount = ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.GET_ACCOUNTS);
        int statusPermissionReadCalendar = ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_CALENDAR);
        if (statusPermissionAccount != PackageManager.PERMISSION_GRANTED || statusPermissionReadCalendar != PackageManager.PERMISSION_GRANTED) {
            String[] listTypeRequest;
            switch (typeRequest) {
                case Statics.REQUEST_CODE_ASK_PERMISSIONS_ALL:
                    listTypeRequest = new String[]{Manifest.permission.GET_ACCOUNTS, Manifest.permission.READ_CALENDAR};
                    requestPermissions(listTypeRequest, typeRequest);
                    break;
                case Statics.REQUEST_CODE_ASK_PERMISSIONS_READ_CALENDAR:
                    listTypeRequest = new String[]{Manifest.permission.READ_CALENDAR};
                    requestPermissions(listTypeRequest, typeRequest);
                    break;

                case Statics.REQUEST_CODE_ASK_PERMISSIONS_READ_CONTAXT:
                    listTypeRequest = new String[]{Manifest.permission.GET_ACCOUNTS};
                    requestPermissions(listTypeRequest, typeRequest);
                    break;
            }

            return;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {
            case Statics.REQUEST_CODE_ASK_PERMISSIONS_ALL:
                getListAccountGoogle();
                mListAccountAdapter.updateListAccount(listAccount);
                break;
            case Statics.REQUEST_CODE_ASK_PERMISSIONS_READ_CALENDAR:
                if (grantResults[0] == 0) {
                    updateUi();
                }
                break;

            case Statics.REQUEST_CODE_ASK_PERMISSIONS_READ_CONTAXT:
                if (grantResults[0] == 0) {
                    chooseAccount();
                    getListAccountGoogle();
                    mListAccountAdapter.updateListAccount(listAccount);
                }
                break;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void mapView() {
        mAdView = (AdView) findViewById(R.id.adView);
        rvAccount = (RecyclerView) findViewById(R.id.rv_account);
        btnChangeAccount = (Button) findViewById(R.id.btn_change_accout);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        progressSync = (ProgressBar) findViewById(R.id.progress_sync);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        rvAccount.setLayoutManager(layoutManager);
    }


    private void initData() {


        if (Util.isNetworkAvailable()) {
            adRequest = new AdRequest.Builder()
                    .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                    .build();

            mAdView.loadAd(adRequest);
        }


        mCredential = GoogleAccountCredential.usingOAuth2(
                getApplicationContext(), Arrays.asList(SCOPES))
                .setBackOff(new ExponentialBackOff())
                .setSelectedAccountName(null);


        mDb = CalendarDatabaseHelper.getsInstance();
        mDatabaseHandler = new DatabaseHandler();

        //add icon menu to open mDrawerLayout
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.setDrawerListener(toggle);
        toggle.syncState();


        this.setTitle(getResources().getString(R.string.main_monthly));
        mCalendarViewWeekly = CalendarViewWeekly.newInstance(7);
        mCalendarMonthlyFragment = CalendarMonthlyFragment.newInstance();

        getListAccountGoogle();

        displayMonthly(true);


        mListAccountAdapter = new ListAccountAdapter(getApplicationContext(), listAccount, this);
        rvAccount.setAdapter(mListAccountAdapter);

    }

    private void setEventListener() {
        btnChangeAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogSeclect();
            }
        });
    }


    /**
     * Called whenever this activity is pushed to the fore ground, such as after
     * a call to onCreate().
     */
    @Override
    protected void onResume() {
        Log.d(LOG_TAG, "[onResume]");
        if (mAdView != null) {
            mAdView.resume();
        }
        super.onResume();
        udpateListAccount();
        checkColorAccountGoogle();
    }


    private void checkColorAccountGoogle() {
        for (AccountCalendar item : listAccount) {
            if (item.isType && item.isCheck && sessionManager.getColor(item.accountName) == 0) {
                sessionManager.setCheckBox(item.accountName,true);
                showDialogPickColor(item.accountName);

            }
        }
    }

    private void udpateListAccount() {
        AccountManager accountManager = AccountManager.get(MainActivity.this);
        Account[] accounts = accountManager.getAccounts();
        for (int i = 0; i < listAccount.size(); i++) {
            if (listAccount.get(i).isType) {
                boolean isHave = false;
                for (int j = 0; j < accounts.length; j++) {
                    if (accounts[j].type.equals(getResources().getString(R.string.main_account_type_google)) && accounts[j].name.equals(listAccount.get(i).accountName)) {
                        isHave = true;
                    }

                }
                if (!isHave) {
                    sessionManager.logoutUser(listAccount.get(i).accountName, true);
                    listAccount.remove(i);
                    updateUi();
                    i--;
                }
            }

        }
        mListAccountAdapter.updateListAccount(listAccount);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_weekly:
                displayMonthly(false);
                return true;
            case R.id.action_month:
                displayMonthly(true);
                return true;
            case R.id.action_refresh:
                syncData();
                return true;
            case R.id.action_change_color:
                showDialogChangColor();
                return true;
            case R.id.action_logout_outlook:
                if (countAccountOutlook() != 0) {
                    logoutAccountOutlook();
                }else{
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.main_text_non_account_outlook),Toast.LENGTH_LONG).show();
                }
                return true;

        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onActivityResult(
            int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(LOG_TAG, "[onActivityResult][requestCode]" + requestCode);
        switch (requestCode) {
            case Statics.REQUEST_GOOGLE_PLAY_SERVICES:
                if (resultCode != RESULT_OK) {
                    isGooglePlayServicesAvailable();
                }
                break;
            case Statics.REQUEST_ACCOUNT_PICKER:
                if (resultCode == RESULT_OK && data != null &&
                        data.getExtras() != null) {
                    String accountName =
                            data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                    if (!checkAccountHaveInList(accountName)) {
                        listAccount.add(new AccountCalendar(accountName, true, true));
                        checkPermissionReadCalendar();
                    } else {
                        updateListAccount(accountName, true);
                        checkPermissionReadCalendar();
                    }
                } else if (resultCode == RESULT_CANCELED) {
                }
                break;
            case Statics.REQUEST_AUTHORIZATION:
                if (resultCode != RESULT_OK) {
                    chooseAccount();
                }
                break;
            case Statics.REQUEST_ACCOUNT_OUTLOOK:
                showDialog(true);
                AuthenticationManager
                        .getInstance()
                        .getAuthenticationContext()
                        .onActivityResult(requestCode, resultCode, data);
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void chooseAccount() {

        if (isDeviceOnline()) {
            if (null != mCredential.getSelectedAccountName()) {
                mCredential.setSelectedAccountName(null);
            }
            startActivityForResult(
                    mCredential.newChooseAccountIntent(), Statics.REQUEST_ACCOUNT_PICKER);
        } else {
            displayMonthly(true);
        }

    }

    /**
     * Checks whether the device currently has a network connection.
     *
     * @return true if the device has a network connection, false otherwise.
     */
    private boolean isDeviceOnline() {
        ConnectivityManager connMgr =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    /**
     * Check that Google Play services APK is installed and up to date. Will
     * launch an error dialog for the user to update Google Play Services if
     * possible.
     *
     * @return true if Google Play Services is available and up to
     * date on this device; false otherwise.
     */
    private boolean isGooglePlayServicesAvailable() {
        final int connectionStatusCode =
                GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (GooglePlayServicesUtil.isUserRecoverableError(connectionStatusCode)) {
            showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode);
            return false;
        } else if (connectionStatusCode != ConnectionResult.SUCCESS) {
            return false;
        }
        return true;
    }

    public void showGooglePlayServicesAvailabilityErrorDialog(
            final int connectionStatusCode) {
        Dialog dialog = GooglePlayServicesUtil.getErrorDialog(
                connectionStatusCode,
                MainActivity.this,
                Statics.REQUEST_GOOGLE_PLAY_SERVICES);
        dialog.show();
    }


    public void showDialog(boolean isShow) {
        if (isShow) {
            progressSync.setVisibility(View.VISIBLE);
        } else {
            progressSync.setVisibility(View.GONE);
        }
    }

    private void addListEventToDb(final List<DataCalendar> list, final String username, final boolean isType) {
        Log.d(LOG_TAG, "[addListEventToDb][mWeekViewEvents]" + list.size());
        Log.d(LOG_TAG, "[addListEventToDb][username]" + username);
        if (!mDb.checkUserInDb(username)) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    MainActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            showDialogPickColor(username, list, isType);

                        }
                    });
                }
            }).start();
        } else {
            addListToDatabase(list, username);
        }
    }

    public void showDialogPickColor(final String username, final List<DataCalendar> list, final boolean isType) {
        AmbilWarnaDialog dialog = new AmbilWarnaDialog(MainActivity.this, Statics.COLOR_DEFAULT, false, new AmbilWarnaDialog.OnAmbilWarnaListener() {
            @Override
            public void onOk(AmbilWarnaDialog dialog, int color) {
                if (isType) {
                    updateListAccount(username, true);
                    checkPermissionReadCalendar();
                    sessionManager.putColor(username, color);
                    updateUi();
                } else {
                    mDatabaseHandler.addAccount(username, color, new RequestCallbackDb<Boolean>() {
                        @Override
                        public void onDbResult(Boolean data) {
                            if (data) {
                                addListToDatabase(list, username);
                            }
                        }
                    });
                }
            }

            @Override
            public void onCancel(AmbilWarnaDialog dialog) {
                if (isType) {
                    updateListAccount(username, true);
                    checkPermissionReadCalendar();
                    sessionManager.putColor(username, Statics.COLOR_DEFAULT);
                    updateUi();
                } else {
                    mDatabaseHandler.addAccount(username, Statics.COLOR_DEFAULT, new RequestCallbackDb<Boolean>() {
                        @Override
                        public void onDbResult(Boolean data) {
                            if (data) {
                                mDb.removeListEvent(username);
                                addListToDatabase(list, username);
                            }
                        }
                    });
                }
            }
        }

        );
        dialog.show();
    }

    private void addListToDatabase(final List<DataCalendar> list, final String username) {
        Log.d(LOG_TAG, "[addListEventToDb][size]" + list.size());
        if (list.size() > 0)
            mDatabaseHandler.addListEvent(list, username, new RequestCallbackDb<Boolean>() {
                @Override
                public void onDbResult(Boolean data) {
                    setSyncAccountSuccess(username);
                    updateUi();
                    if (checkSyncAll()) {
                        progressSync.setVisibility(View.GONE);
                    }
                    mListAccountAdapter.updateListAccount(listAccount);
                }
            });
    }

    private void setSyncAccountSuccess(String username) {
        for (AccountCalendar item : listAccount) {
            if (item.accountName.equals(username)) {
                item.isSync = false;
                break;
            }
        }
    }

    private boolean checkSyncAll() {
        for (AccountCalendar item : listAccount) {
            if (item.isSync) {
                return false;
            }
        }
        return true;
    }

    private void updateListAccount(String usename, boolean isCheck) {

        for (AccountCalendar account : listAccount) {
            if (usename.equals(account.accountName)) {
                account.isCheck = isCheck;
            }
        }
    }

    public List<DataCalendar> getListEventInMoth(int month, int year) {
        List<DataCalendar> list = new ArrayList<>();
        for (AccountCalendar item : listAccount) {
            if (item.isCheck) {
                if (!item.isType) {
                    list.addAll(mDb.getEventInMonth(month, year, item.accountName, mDb.getColorAccount(item.accountName)));
                } else {
                    list.addAll(getEventByCalendarProvider(item.accountName, month, year, sessionManager.getColor(item.accountName)));
                }
            }
        }
        return list;
    }


    public List<WeekViewEvent> checkEventInDate(int month, int year) {
        List<WeekViewEvent> mListEventInDate = new ArrayList<>();
        List<DataCalendar> listEvent = getListEventInMoth(month, year);
        for (int i = 0; i < listEvent.size(); i++) {
            WeekViewEvent event = new WeekViewEvent(i, listEvent.get(i).title, listEvent.get(i).startDate, listEvent.get(i).endDate);
            if ((i % 3) == 0) {
                event.setColor(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent));
            } else {
                if ((i % 3) == 1)
                    event.setColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimaryDark));
                else {
                    event.setColor(ContextCompat.getColor(getApplicationContext(), R.color.color_event_three));

                }
            }
            mListEventInDate.add(event);
        }

        return mListEventInDate;
    }

    private void addFragmentToActivity(FragmentManager fragmentManager, Fragment fragment, int frameLayout, boolean isSaveStack) {

        FragmentTransaction transaction = fragmentManager.beginTransaction();

        transaction.add(frameLayout, fragment);

        if (isSaveStack) {
            transaction.addToBackStack(fragment.getId() + "");
        }

        transaction.commitAllowingStateLoss();
    }

    private void displayMonthly(boolean isDisplay) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        if (isDisplay) {
            this.setTitle(getResources().getString(R.string.main_monthly));
            if (mCalendarMonthlyFragment.isAdded()) {
                ft.hide(mCalendarViewWeekly);
                ft.show(mCalendarMonthlyFragment);
            } else {
                ft.hide(mCalendarViewWeekly);
                ft.add(R.id.content_fragment, mCalendarMonthlyFragment);
                ft.show(mCalendarMonthlyFragment);
            }
        } else {
            this.setTitle(getResources().getString(R.string.main_weekly));
            if (mCalendarViewWeekly.isAdded()) {
                ft.hide(mCalendarMonthlyFragment);
                ft.show(mCalendarViewWeekly);
            } else {
                ft.add(R.id.content_fragment, mCalendarViewWeekly);
                ft.show(mCalendarViewWeekly);
                ft.hide(mCalendarMonthlyFragment);
            }
        }

        if (mDetailCalendarFragment != null && mDetailCalendarFragment.isVisible()) {
            ft.remove(mDetailCalendarFragment);
            getSupportFragmentManager().popBackStack();
        }
        ft.commit();
        mDrawerLayout.closeDrawer(GravityCompat.START);
    }

    @Override
    public void onBackPressed() {

        if (mCalendarMonthlyFragment.isVisible()) {
            this.setTitle(getResources().getString(R.string.main_monthly));
        } else {
            this.setTitle(getResources().getString(R.string.main_weekly));
        }
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onSelectDateInCalendar(int date, int month, int year) {
        this.setTitle(getResources().getString(R.string.main_daily));
        mDetailCalendarFragment = DetailCalendarFragment.newInstance(date, month, year);
        addFragmentToActivity(getSupportFragmentManager(), mDetailCalendarFragment, R.id.content_fragment, true);
    }

    @Override
    public void onSelectDateInWeekly(Calendar calendar) {
        this.setTitle(getResources().getString(R.string.main_daily));
        mDetailCalendarFragment = DetailCalendarFragment.newInstance(calendar.get(Calendar.DATE), calendar.get(Calendar.MONTH), calendar.get(Calendar.YEAR));
        addFragmentToActivity(getSupportFragmentManager(), mDetailCalendarFragment, R.id.content_fragment, true);
    }


    @Override
    public void onSelectAccount(String userName, boolean isCheck) {
        if (isCheck) {
            if (!checkTypeAccount(userName)) {
                mDb.updateCheck(userName, true);
                updateListAccount(userName, true);
                updateUi();
            } else {
                sessionManager.setCheckBox(userName, true);
                if (sessionManager.getColor(userName) == 0) {
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                    showDialogPickColor(userName, null, true);
                } else {
                    updateListAccount(userName, true);
                    checkPermissionReadCalendar();
                }
            }
        } else {
            if (checkTypeAccount(userName)) {
                sessionManager.setCheckBox(userName, false);
            } else {
                mDb.updateCheck(userName, false);
            }
            updateListAccount(userName, false);
            updateUi();
        }
    }

    private boolean checkTypeAccount(String usename) {
        for (AccountCalendar item : listAccount) {
            if (item.accountName.equals(usename)) {
                return item.isType;
            }
        }
        return false;
    }

    public void updateUi() {
        if (!mCalendarMonthlyFragment.isHidden()) {
            Log.d(LOG_TAG, "[onSelectAccount][Add account][mCalendarMonthlyFragment]");

            mCalendarMonthlyFragment.updateListEvent();
        } else {
            Log.d(LOG_TAG, "[onSelectAccount][Add account][mCalendarViewWeekly]");

            mCalendarViewWeekly.updateListEvent();
        }

        if (mDetailCalendarFragment != null && mDetailCalendarFragment.isVisible()) {
            mDetailCalendarFragment.updateListEvent();
        }
    }


    private boolean checkAccountHaveInList(String usename) {
        if (listAccount != null && listAccount.size() > 0) {
            for (AccountCalendar item : listAccount) {
                if (usename.equals(item.accountName))
                    return true;

            }
        }
        return false;
    }


    private Calendar getCalendarFromDateTime(Date dateTime) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateTime);
        return calendar;
    }

    private void syncData() {
        setSyncForAccountCheck();
        for (final AccountCalendar item : listAccount) {
            if (item.isCheck) {
                if (item.isType) {
                    updateUi();
                    item.isSync = false;
                    if (checkSyncAll()) {
                        progressSync.setVisibility(View.GONE);
                    }
                } else {
                    progressSync.setVisibility(View.VISIBLE);
                    Log.d(LOG_TAG, "[syncData]" + AuthenticationSettings.INSTANCE.getSecretKeyData());
                    if (!TextUtils.isEmpty(sessionManager.getAccessToken(item.accountName))) {
                        callOutLookEvent(item.accountName);
                    } else {
                        addNewOutlook();
                    }

                }
            }
        }

    }

    private void callOutLookEvent(final String usename) {
        mWebServiceHandler.getEventFromOffice365(usename, new RequestCallback<List<DataCalendar>>() {
            @Override
            public void onSuccess(List<DataCalendar> data) {
                addListToDatabase(data, usename);
            }

            @Override
            public void onError() {
                refreshTokenOutLook(usename);
            }
        });
    }

    private void refreshTokenOutLook(final String usename) {
        mWebServiceHandler.refreshToken(usename, new RequestCallback<Boolean>() {
            @Override
            public void onSuccess(Boolean data) {
                callOutLookEvent(usename);
            }

            @Override
            public void onError() {
                showDialog(false);
                mDb.removeAccount(usename);
                sessionManager.logoutUser(usename, false);
            }
        });
    }

    private void setSyncForAccountCheck() {
        int flagAccountCheck = 0;
        for (AccountCalendar item : listAccount) {
            if (item.isCheck) {
                {
                    item.isSync = true;
                    flagAccountCheck++;
                }
            }

        }
        if (flagAccountCheck == 0) {
            progressSync.setVisibility(View.GONE);

        }
    }

    private void showDialogSeclect() {
        mDrawerLayout.closeDrawer(GravityCompat.START);


        CharSequence[] colors = new CharSequence[]{getResources().getString(R.string.main_add_account_google), getResources().getString(R.string.main_add_account_outlook)};


        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.main_title_add_account));
        builder.setItems(colors, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int log) {
                if (log == 0) {
                    if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.GET_ACCOUNTS) == PackageManager.PERMISSION_DENIED) {
                        insertDummyContactWrapper(Statics.REQUEST_CODE_ASK_PERMISSIONS_READ_CONTAXT);
                    } else {
                        chooseAccount();
                    }
                } else {
                    addNewOutlook();

                }
            }
        });
        builder.show();
    }

    private void showDialogChangColor() {
        CharSequence[] account = new CharSequence[listAccount.size()];
        for (int i = 0; i < listAccount.size(); i++) {
            account[i] = listAccount.get(i).accountName;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.main_title_change_color));
        builder.setItems(account, new DialogInterface.OnClickListener() {
            @Override

            public void onClick(DialogInterface dialog, int which) {
                showDialogPickColor(listAccount.get(which).accountName);
            }
        });
        builder.show();
    }

    private void showDialogPickColor(final String usename) {
        AmbilWarnaDialog dialog = new AmbilWarnaDialog(MainActivity.this, Statics.COLOR_DEFAULT, false, new AmbilWarnaDialog.OnAmbilWarnaListener() {
            @Override
            public void onOk(AmbilWarnaDialog dialog, int color) {
                if (checkTypeAccount(usename)) {
                    sessionManager.putColor(usename, color);
                } else {
                    mDb.updateColor(usename, color);
                }
                updateUi();
            }

            @Override
            public void onCancel(AmbilWarnaDialog dialog) {
            }
        });
        dialog.show();

    }

    @Override
    protected void onStop() {
        Log.d(LOG_TAG, "[onStop]");

        super.onStop();
    }

    private List<DataCalendar> getEventByCalendarProvider(String userName, int month, int year, final int color) {
        List<DataCalendar> listEvent = new ArrayList<>();

        ContentResolver cr = getContentResolver();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {

            }
        }

        String selection = "((" + CalendarContract.Events.ACCOUNT_NAME + "=\'" + String.valueOf(userName) + "\') AND ("
                + CalendarContract.Events.DTSTART + " >= " + Util.getStartMonth(month, year) + ") AND ("
                + CalendarContract.Events.DTSTART + " <= " + Util.getEndMonth(month, year) + "))";
        Cursor cursor = cr.query(CalendarContract.Events.CONTENT_URI, null, selection, null, null);
        if (cursor.moveToFirst()) {
            do {
                {
                    DataCalendar dataCalendar = new DataCalendar();
                    dataCalendar.title = TextUtils.isEmpty(cursor.getString(cursor.getColumnIndex(CalendarContract.Events.TITLE))) ? "no title" : cursor.getString(cursor.getColumnIndex(CalendarContract.Events.TITLE));
                    TimeZone timeZone = java.util.TimeZone.getTimeZone(cursor.getString(cursor.getColumnIndex(CalendarContract.Events.EVENT_TIMEZONE)));
                    dataCalendar.startDate = Util.getTime(cursor.getLong(cursor.getColumnIndex(CalendarContract.Events.DTSTART)), timeZone);
                    dataCalendar.endDate = Util.getTime(cursor.getLong(cursor.getColumnIndex(CalendarContract.Events.DTEND)), timeZone);
                    dataCalendar.account = userName;
                    dataCalendar.color = color;
                    listEvent.add(dataCalendar);
                }
            } while (cursor.moveToNext());
        }
        return listEvent;
    }

    private void checkPermissionReadCalendar() {
        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_CALENDAR) == PackageManager.PERMISSION_DENIED) {
            if (Integer.valueOf(Build.VERSION.SDK_INT) > 22) {
                insertDummyContactWrapper(Statics.REQUEST_CODE_ASK_PERMISSIONS_READ_CALENDAR);
            }
        } else {
            updateUi();
        }
    }

    private void showBanner(boolean isShow) {
        if (isShow) {
            if (adRequest == null) {
                adRequest = new AdRequest.Builder()
                        .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                        .build();

                mAdView.loadAd(adRequest);
            }

            mAdView.setVisibility(View.VISIBLE);
        } else {
            mAdView.setVisibility(View.GONE);
        }

    }

    private void addNewOutlook() {
        try {
            UUID.fromString(Statics.CLIENT_ID);
            URI.create(Statics.REDIRECT_URI);
        } catch (IllegalArgumentException e) {
            Toast.makeText(
                    this
                    , getString(R.string.warning_clientid_redirecturi_incorrect)
                    , Toast.LENGTH_LONG).show();

            return;
        }

        AuthenticationManager.getInstance().setContextActivity(this);

        AuthenticationManager.getInstance().connect(
                new AuthenticationCallback<AuthenticationResult>() {
                    @Override
                    public void onSuccess(AuthenticationResult authenticationResult) {
                        accountOutlook = authenticationResult.getUserInfo().getDisplayableId();
                        getEventOutlook();
                    }

                    @Override
                    public void onError(final Exception e) {
                        Log.d(LOG_TAG, "onCreate - " + e.getMessage());
                        showDialog(false);
                    }
                });
    }

    private void getEventOutlook() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                AuthenticationManager.getInstance().setResourceId(Statics.RESOURCE_ID);
                ADALDependencyResolver dependencyResolver = (ADALDependencyResolver) AuthenticationManager.getInstance().getDependencyResolver();
                OutlookClient eventClient = new OutlookClient(Statics.SERVICE_ENDPOIN_URI, dependencyResolver);
                Futures.addCallback(eventClient.getMe().getCalendar().getEvents().read(), new FutureCallback<List<Event>>() {
                    @Override
                    public void onSuccess(List<Event> result) {
                        List<DataCalendar> list = new ArrayList<DataCalendar>();
                        if (!checkAccountHaveInList(accountOutlook)) {
                            listAccount.add(new AccountCalendar(accountOutlook, true, false));
                        } else {
                            updateListAccount(accountOutlook, true);

                        }

                        Log.d(LOG_TAG, "[getEventOutlook][size]" + result.size());
                        for (Event item : result) {
                            Log.d(LOG_TAG, "[getEventOutlook][title]" + item.getSubject());
                            DataCalendar temp = new DataCalendar();
                            temp.account = accountOutlook;
                            temp.startDate = getCalendarFromDateTime(item.getStart().getTime());
                            temp.endDate = getCalendarFromDateTime(item.getEnd().getTime());
                            temp.title = item.getSubject();
                            list.add(temp);
                        }
                        addListEventToDb(list, accountOutlook, false);
                        getTokenOutlook();
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        Log.d(LOG_TAG, "[getEventOutlook][onFailure]" + t.getMessage());
                        showDialog(false);
                    }
                });
            }
        }).start();
    }


    private void getTokenOutlook() {

        try {
            UUID.fromString(Statics.CLIENT_ID);
            URI.create(Statics.REDIRECT_URI);
        } catch (IllegalArgumentException e) {
            Toast.makeText(
                    this
                    , getString(R.string.warning_clientid_redirecturi_incorrect)
                    , Toast.LENGTH_LONG).show();

            return;
        }
        AuthenticationManager.getInstance().setContextActivity(this);
        AuthenticationManager.getInstance().connect(
                new AuthenticationCallback<AuthenticationResult>() {
                    @Override
                    public void onSuccess(AuthenticationResult authenticationResult) {
                        sessionManager.createNewToken(authenticationResult.getUserInfo().getDisplayableId(), authenticationResult.getAccessToken(), authenticationResult.getRefreshToken());
                        AuthenticationManager.getInstance().disconnect();
                        showDialog(false);
                    }


                    @Override
                    public void onError(final Exception e) {
                        Log.d(LOG_TAG, "onCreate - " + e.getMessage());
                        showDialog(false);
                    }
                });

    }

    private int countAccountOutlook()

    {
        int countAccount = 0;
        for (int i = 0; i < listAccount.size(); i++) {
            if (!listAccount.get(i).isType) {
                countAccount++;
            }
        }
        return countAccount;
    }

    private void logoutAccountOutlook() {
        int i = 0;
        final CharSequence[] account = new CharSequence[countAccountOutlook()];
        for (AccountCalendar item : listAccount) {
            if (!item.isType) {
                account[i] = item.accountName;
                i++;
            }
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.main_title_logout_outlook));
        builder.setItems(account, new DialogInterface.OnClickListener() {
            @Override

            public void onClick(DialogInterface dialog, int which) {
                removeAccount(account[which].toString());
                mDb.removeListEvent(account[which].toString());
                mDb.removeAccount(account[which].toString());
                mListAccountAdapter.notifyDataSetChanged();
                updateUi();
            }
        });
        builder.show();
    }

    private void removeAccount(String usename) {
        for (AccountCalendar item : listAccount) {
            if (item.accountName.equals(usename)) {
                listAccount.remove(item);
                break;
            }
        }
    }

    @Override
    protected void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        if (mBroadcastReceiverEventUpdate != null) {
            unregisterReceiver(mBroadcastReceiverEventUpdate);
            mBroadcastReceiverEventUpdate = null;
        }

        if (mBroadcastReceiverNetWorkChange != null) {
            unregisterReceiver(mBroadcastReceiverNetWorkChange);
            mBroadcastReceiverNetWorkChange = null;
        }
        super.onDestroy();
    }

    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }


}
