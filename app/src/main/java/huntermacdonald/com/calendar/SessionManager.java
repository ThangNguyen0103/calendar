package huntermacdonald.com.calendar;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by quoctran on 04/12/2015.
 */
public class SessionManager {

    private SharedPreferences pref;

    private SharedPreferences.Editor editor;

    private static final String PREF_NAME = "CalendarSessionManager";

    private int PRIVATE_MODE = 0;

    private static final String KEY_COLOR = "is_color_";
    private static final String KEY_ACCESS_TOKEN = "is_access_token_";
    private static final String KEY_REFRESH_TOKEN = "is_refresh_token_";
    private static final String KEY_CHECK_BOX = "is_check";

    public SessionManager(Context context) {
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setCheckBox(String usename,boolean isCheck){
        editor.putBoolean(KEY_CHECK_BOX+usename,isCheck);
        editor.commit();
    }

    public boolean getCheckBox(String usename)
    {
        return pref.getBoolean(KEY_CHECK_BOX+usename,false);
    }

    public int getColor(String usename) {
        return pref.getInt(KEY_COLOR + usename, 0);
    }

    public String getRefreshToken(String usename) {
        return pref.getString(KEY_REFRESH_TOKEN + usename, null);
    }

    public String getAccessToken(String usename) {
        return pref.getString(KEY_ACCESS_TOKEN + usename, null);
    }

    public void createNewToken(String usename, String accessToken, String refreshToken) {
        editor.putString(KEY_REFRESH_TOKEN + usename, refreshToken);
        editor.putString(KEY_ACCESS_TOKEN + usename, accessToken);
        editor.commit();
    }

    public void putColor(String usename, int color) {
        editor.putInt(KEY_COLOR + usename, color);
        editor.commit();
    }

    public void logoutUser(String usename, boolean isType) {
        if (isType) {
            editor.putInt(KEY_COLOR + usename, 0);
            editor.putBoolean(KEY_CHECK_BOX+usename,false);
        } else {
            editor.putString(KEY_REFRESH_TOKEN + usename, null);
            editor.putString(KEY_ACCESS_TOKEN + usename, null);
        }
        editor.commit();
    }

}
