package huntermacdonald.com.calendar.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import huntermacdonald.com.calendar.R;
import huntermacdonald.com.calendar.model.AccountCalendar;

/**
 * Created by quoctran on 26/10/2015.
 */
public class ListAccountAdapter extends RecyclerView.Adapter<ListAccountAdapter.ViewHolder> {


    private List<AccountCalendar> mListAccount;
    private Context mContext;
    private OnListenerAccountSelect mListener;

    public ListAccountAdapter(Context context, List<AccountCalendar> list, ListAccountAdapter.OnListenerAccountSelect listener) {
        mContext = context;
        mListAccount = list;
        this.mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_list_account, parent, false);
        ViewHolder holder = new ViewHolder(v);

        return holder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.txtItemEmail.setText(mListAccount.get(position).accountName);

        holder.cbEmail.setChecked(mListAccount.get(position).isCheck);

        holder.cbEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onSelectAccount(mListAccount.get(position).accountName, holder.cbEmail.isChecked());
            }
        });


    }

    @Override
    public int getItemCount() {
        return mListAccount.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public CheckBox cbEmail;
        public TextView txtItemEmail;

        public ViewHolder(View view) {
            super(view);

            cbEmail = (CheckBox) view.findViewById(R.id.cb_item_email_checkbox);
            txtItemEmail = (TextView) view.findViewById(R.id.txt_item_email_account);
        }
    }

    public void updateListAccount(List<AccountCalendar> listAccount) {
        this.mListAccount = listAccount;
        notifyDataSetChanged();
    }

    public interface OnListenerAccountSelect {
        void onSelectAccount(String userName, boolean isCheck);
    }
}
