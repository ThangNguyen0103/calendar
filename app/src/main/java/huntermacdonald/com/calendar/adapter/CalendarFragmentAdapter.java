package huntermacdonald.com.calendar.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;
import android.view.ViewGroup;

import java.util.Calendar;

import huntermacdonald.com.calendar.fragment.CalendarFragment;

/**
 * Created by quoctran on 19/10/2015.
 */
public class CalendarFragmentAdapter extends FragmentPagerAdapter {

    private final String LOG_TAG = "CalendarFragmentAdapter";

    private Context mContext;
    private CalendarFragment mCalendarFragment;


    public CalendarFragmentAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;

    }


    @Override
    public Fragment getItem(int position) {
        Log.d(LOG_TAG, "[getItem][position]" + position);
        Calendar calendar = Calendar.getInstance();
        if (position == 1000) {

            mCalendarFragment = CalendarFragment.newInstance(calendar.get(Calendar.MONTH), calendar.get(Calendar.YEAR));
        } else {
            calendar.add(Calendar.MONTH, position - 1000);
            mCalendarFragment = CalendarFragment.newInstance(calendar.get(Calendar.MONTH), calendar.get(Calendar.YEAR));
        }
        return mCalendarFragment;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    @Override
    public int getItemPosition(Object object) {
        CalendarFragment f = (CalendarFragment) object;
        if (f != null) {
            f.updateData();
        }
        return super.getItemPosition(object);
    }

    @Override
    public int getCount() {
        return 2000;
    }

}
