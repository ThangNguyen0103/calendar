package huntermacdonald.com.calendar.adapter;

import android.app.ActionBar;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import huntermacdonald.com.calendar.R;
import huntermacdonald.com.calendar.model.DataCalendar;

/**
 * Created by quoctran on 21/10/2015.
 */
public class TimeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private Context mContext;
    private List<DataCalendar> mListDataCalendars = new ArrayList<>();
    private final String[] mListTime12h = {"12 AM", "1 AM", "2 AM", "3 AM", "4 AM", "5 AM", "6 AM", "7 AM", "8 AM", "9 AM", "10 AM", "11 AM", "12 PM",
            "1 PM", "2 PM", "3 PM", "4 PM", "5 PM", "6 PM", "7 PM", "8 PM", "9 PM", "10 PM", "11 PM"};

    public TimeAdapter(Context context, List<DataCalendar> list) {
        mContext = context;
        mListDataCalendars = list;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        RecyclerView.ViewHolder holder;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_time, parent, false);
        holder = new ItemViewHolder(view,3, mContext);

        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
        itemViewHolder.txtTitleHour.setText(mListTime12h[position]);
        if (mListDataCalendars.size() > 0) {

        }

    }

    @Override
    public int getItemCount() {
        return mListTime12h.length;
    }


    public static class ItemViewHolder extends RecyclerView.ViewHolder {

        public TextView txtTitleHour;
        public LinearLayout layoutHourSumary;

        public ItemViewHolder(View view, int mCount, Context context) {
            super(view);

            ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

            layoutHourSumary = (LinearLayout) view.findViewById(R.id.layout_hour_sumary);
            txtTitleHour = (TextView) view.findViewById(R.id.txt_title_hour);


            for (int i = 0; i < mCount; i++) {
                TextView textView = new TextView(context);
                textView.setId(i);
                textView.setBackgroundColor(ContextCompat.getColor(context,R.color.colorAccent));
                textView.setLayoutParams(layoutParams);
                layoutHourSumary.addView(textView);
            }
        }
    }

}
