package huntermacdonald.com.calendar.adapter;

import android.content.Context;
import android.database.DataSetObserver;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import huntermacdonald.com.calendar.R;
import huntermacdonald.com.calendar.model.DateCalendar;

/**
 * Created by quoctran on 19/10/2015.
 */
public class CalendarAdapter extends BaseAdapter {

    private final String LOG_TAG = "CalendarAdapter";

    private List<DateCalendar> listDate = new ArrayList<>();
    private Context context;
    private boolean currentDate;
    private int currentMonth;
    private int currentYear;
    private OnClickDateInCalendar mListener;


    public CalendarAdapter(Context context, OnClickDateInCalendar listener, List<DateCalendar> list, boolean currentDate, int currentMonth, int currentYear) {
        this.context = context;
        this.listDate = list;
        this.currentDate = currentDate;
        this.currentMonth = currentMonth;
        this.currentYear = currentYear;
        this.mListener = listener;

    }

    @Override
    public int getCount() {
        return listDate.size();
    }

    @Override
    public Object getItem(int position) {
        return listDate.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.cells_date, null);
        }
        LinearLayout layoutContent = (LinearLayout) convertView.findViewById(R.id.content_layout);

        TextView txtDate = (TextView) convertView.findViewById(R.id.calendar_day);
        TextView txtTitle1 = (TextView) convertView.findViewById(R.id.txt_title_calendar1);
        TextView txtTitle2 = (TextView) convertView.findViewById(R.id.txt_title_calendar2);
        TextView txtTitle3 = (TextView) convertView.findViewById(R.id.txt_title_calendar3);
        LinearLayout layoutTitle = (LinearLayout) convertView.findViewById(R.id.layout_title);

        if (listDate.get(position).titleObject.size() > 0 && dateInMonth(position, currentMonth, currentYear)) {
            setEventIntoCalendar(listDate.get(position).titleObject, txtTitle1, txtTitle2, txtTitle3);
            layoutTitle.setVisibility(View.VISIBLE);
        } else {
            layoutTitle.setVisibility(View.INVISIBLE);
        }
        Calendar calendar = Calendar.getInstance();

        if (dateInMonth(position, currentMonth, currentYear)) {
            txtDate.setText(listDate.get(position).date + "");
            txtDate.setTextColor(ContextCompat.getColor(context, R.color.colorBlack));
        } else {
            txtDate.setText(listDate.get(position).date + "");
            txtDate.setTextColor(ContextCompat.getColor(context, R.color.colorDate));
        }

        if (currentDate) {
            if (calendar.get(Calendar.DATE) == listDate.get(position).date && dateInMonth(position, currentMonth, currentYear)) {
                txtDate.setBackgroundResource(R.drawable.border_today);
                txtDate.setTextColor(ContextCompat.getColor(context, R.color.color_white));
            }
        }

        layoutContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkDateInMonth(position, currentMonth, currentYear);
            }
        });
        if (position == listDate.size() - 1) {
            mListener.onCloseDialog();
        }
        return convertView;
    }

    private void checkDateInMonth(int position, int month, int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, 1);
        int startMonth = calendar.get(Calendar.DAY_OF_WEEK) - 1;
        int endMonth = startMonth + calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        if (startMonth <= position && position < endMonth) {
            mListener.onClickDate(listDate.get(position).date, month, year);
        } else {
            if (position <= startMonth) {
                mListener.onClickDate(listDate.get(position).date, month - 1, year);
            } else {
                mListener.onClickDate(listDate.get(position).date, month + 1, year);

            }
        }
    }


    private boolean dateInMonth(int position, int month, int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, 1);
        int startMonth = calendar.get(Calendar.DAY_OF_WEEK) - 1;
        int endMonth = startMonth + calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        if (startMonth <= position && position < endMonth) {
            return true;
        }
        return false;
    }

    private void setEventIntoCalendar(List<DateCalendar.TitleCalendar> listEvent, TextView event1, TextView event2, TextView event3) {
        if (listEvent.size() > 0) {
            switch (listEvent.size()) {
                case 1:
                    event2.setVisibility(View.INVISIBLE);
                    event3.setVisibility(View.INVISIBLE);
                    event1.setText(listEvent.get(0).title);
                    event1.setBackgroundColor(listEvent.get(0).color);
                    break;
                case 2:
                    event3.setVisibility(View.INVISIBLE);
                    event2.setVisibility(View.VISIBLE);
                    event1.setText(listEvent.get(0).title);
                    event1.setBackgroundColor(listEvent.get(0).color);
                    event2.setText(listEvent.get(1).title);
                    event2.setBackgroundColor(listEvent.get(1).color);

                    break;
                case 3:
                    event1.setVisibility(View.VISIBLE);
                    event2.setVisibility(View.VISIBLE);
                    event3.setVisibility(View.VISIBLE);
                    event1.setText(listEvent.get(0).title);
                    event1.setBackgroundColor(listEvent.get(0).color);
                    event2.setText(listEvent.get(1).title);
                    event2.setBackgroundColor(listEvent.get(1).color);
                    event3.setText(listEvent.get(2).title);
                    event3.setBackgroundColor(listEvent.get(2).color);



                    break;
                default:
                    event1.setVisibility(View.VISIBLE);
                    event2.setVisibility(View.VISIBLE);
                    event3.setVisibility(View.VISIBLE);
                    event1.setText(listEvent.get(0).title);
                    event1.setBackgroundColor(listEvent.get(0).color);
                    event2.setText(".......");
                    event2.setBackgroundColor(listEvent.get(1).color);
                    event3.setText(listEvent.get(2).title);
                    event3.setBackgroundColor(listEvent.get(2).color);

                    break;
            }
        }
    }


    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {
        super.registerDataSetObserver(observer);
    }

    public interface OnClickDateInCalendar {
        void onClickDate(int date, int month, int year);

        void onCloseDialog();
    }
}