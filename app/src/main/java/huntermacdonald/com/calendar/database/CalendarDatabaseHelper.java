package huntermacdonald.com.calendar.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import huntermacdonald.com.calendar.CalendarApplication;
import huntermacdonald.com.calendar.model.AccountCalendar;
import huntermacdonald.com.calendar.model.DataCalendar;
import huntermacdonald.com.calendar.utils.Util;

/**
 * Created by quoctran on 27/10/2015.
 */
public class CalendarDatabaseHelper extends SQLiteOpenHelper {
    private final String LOG_TAG = "CalendarDatabaseHelper";

    private static CalendarDatabaseHelper sInstance;

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "calendar.db";
    private static final String COLUMN_ID = "_id";

    //table account
    private static final String TABLE_ACCOUNT = "account";

    private static final String COLUMN_ACCOUNT_NAME = "account_name";
    private static final String COLUMN_ACCOUNT_CHECK = "account_check";
    private static final String COLUMN_ACCOUNT_COLOR = "account_color";


    private static final String CREATE_TABLE_ACCOUNT = "create table "
            + TABLE_ACCOUNT
            + " ("
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_ACCOUNT_NAME + " text, "
            + COLUMN_ACCOUNT_CHECK + " integer, "
            + COLUMN_ACCOUNT_COLOR + " integer"
            + ");";


    //table event
    private static final String TABLE_EVENT = "event";

    private static final String COLUMN_EVENT_ACCOUNT = "account";
    private static final String COLUMN_EVENT_SUMARY = "sumary";
    private static final String COLUMN_EVENT_START_DATE = "start_date";
    private static final String COLUMN_EVENT_END_DATE = "end_date";
    private static final String CREATE_TABLE_EVENT = "create table "
            + TABLE_EVENT
            + " ("
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_EVENT_ACCOUNT + " text not null, "
            + COLUMN_EVENT_SUMARY + " text, "
            + COLUMN_EVENT_START_DATE + " long, "
            + COLUMN_EVENT_END_DATE + " long "
            + ");";


    private CalendarDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static synchronized CalendarDatabaseHelper getsInstance() {
        if (sInstance == null) {
            sInstance = new CalendarDatabaseHelper(CalendarApplication.getInstance().getApplicationContext());
        }
        return sInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_ACCOUNT);
        db.execSQL(CREATE_TABLE_EVENT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ACCOUNT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_EVENT);
    }


    //add event to db
    public void insertEvent(DataCalendar event) {
        SQLiteDatabase db = this.getWritableDatabase();
        String nullColumnHack = null;
        ContentValues values = new ContentValues();
        values.put(COLUMN_EVENT_ACCOUNT, event.account);
        values.put(COLUMN_EVENT_SUMARY, event.title);
        values.put(COLUMN_EVENT_START_DATE, event.startDate.getTimeInMillis());
        values.put(COLUMN_EVENT_END_DATE, event.endDate.getTimeInMillis());
        db.insert(TABLE_EVENT, nullColumnHack, values);

    }


    public void removeListEvent(String userName) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = COLUMN_EVENT_ACCOUNT + "=?";

        String[] whereArgs = {userName};
        db.delete(TABLE_EVENT, query, whereArgs);
    }

    public boolean checkUserInDb(String userName) {
        Log.d(LOG_TAG, "[checkUserInDb]");
        boolean isHave = false;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_ACCOUNT, new String[]{COLUMN_ACCOUNT_NAME}, COLUMN_ACCOUNT_NAME + " = ?", new String[]{userName}, null, null, null, null);
        isHave = cursor.moveToFirst();
        cursor.close();
        return isHave;
    }

    public List<AccountCalendar> getAccountOutlook() {
        List<AccountCalendar> listAccount = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT *FROM " + TABLE_ACCOUNT;
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {

                Log.d(LOG_TAG, "[account name]" + cursor.getString(cursor.getColumnIndex(COLUMN_ACCOUNT_NAME)));
                AccountCalendar accountCalendar = new AccountCalendar();
                accountCalendar.accountName = cursor.getString(cursor.getColumnIndex(COLUMN_ACCOUNT_NAME));
                accountCalendar.isType = false;
                accountCalendar.isCheck = (cursor.getInt(cursor.getColumnIndex(COLUMN_ACCOUNT_CHECK)) == 1) ? true : false;
                accountCalendar.isSync = false;
                listAccount.add(accountCalendar);
            }
            while (cursor.moveToNext());
        }
        cursor.close();
        return listAccount;
    }

    public List<DataCalendar> getEventInMonth(int month, int year, String user, int color) {
        Log.d(LOG_TAG, "[getEventInMonth][month]" + month + "[year]" + year);

        List<DataCalendar> listEvent = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT *FROM " + TABLE_EVENT + " WHERE " + COLUMN_EVENT_ACCOUNT + "=\'" + String.valueOf(user) + "\'" + " AND "
                + COLUMN_EVENT_START_DATE + " >= " + Util.getStartMonth(month, year) + " AND "
                + COLUMN_EVENT_START_DATE + " <= " + Util.getEndMonth(month, year);

        Log.d(LOG_TAG, "[getEventInMonth][select query]" + selectQuery);
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                DataCalendar dataCalendar = new DataCalendar();
                dataCalendar.title = cursor.getString(cursor.getColumnIndex(COLUMN_EVENT_SUMARY));
                dataCalendar.startDate = Util.getTime(cursor.getLong(cursor.getColumnIndex(COLUMN_EVENT_START_DATE)), TimeZone.getDefault());
                dataCalendar.endDate = Util.getTime(cursor.getLong(cursor.getColumnIndex(COLUMN_EVENT_END_DATE)), TimeZone.getDefault());
                dataCalendar.account = cursor.getString(cursor.getColumnIndex(COLUMN_EVENT_ACCOUNT));
                dataCalendar.color = color;
                Log.d(LOG_TAG, "[getListEvent][title]" + dataCalendar.title + "[account]" + dataCalendar.account);
                listEvent.add(dataCalendar);
            } while (cursor.moveToNext());
        }
        Log.d(LOG_TAG, "[getEventInMonth][size]" + listEvent.size());

        cursor.close();
        return listEvent;
    }


    public void insertAccount(String account, int color) {
        Log.d(LOG_TAG, "[insertAccount][account]" + account);
        SQLiteDatabase db = this.getWritableDatabase();
        String nullColumnHack = null;
        ContentValues values = new ContentValues();
        values.put(COLUMN_ACCOUNT_NAME, account);
        values.put(COLUMN_ACCOUNT_CHECK, 1);
        values.put(COLUMN_ACCOUNT_COLOR, color);
        db.insert(TABLE_ACCOUNT, nullColumnHack, values);
    }

    public int getColorAccount(String username) {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT *FROM " + TABLE_ACCOUNT + " WHERE " + COLUMN_ACCOUNT_NAME + "=\'" + String.valueOf(username) + "\'";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            int color =cursor.getInt(cursor.getColumnIndex(COLUMN_ACCOUNT_COLOR));
            cursor.close();
            return color;
        }
        cursor.close();
        return 0;
    }

    public void removeAccount(String username) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = COLUMN_ACCOUNT_NAME + "=?";

        String[] whereArgs = {username};
        db.delete(TABLE_ACCOUNT, query, whereArgs);
    }

    public void updateColor(String usename, int color) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_ACCOUNT_COLOR, color);
        db.update(TABLE_ACCOUNT, values, COLUMN_ACCOUNT_NAME + " = ?", new String[]{String.valueOf(usename)});
    }

    public void updateCheck(String usename, boolean isCheck) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_ACCOUNT_CHECK, isCheck ? 1 : 0);
        db.update(TABLE_ACCOUNT, values, COLUMN_ACCOUNT_NAME + " = ?", new String[]{String.valueOf(usename)});
    }


}
