package huntermacdonald.com.calendar.database;


import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;

import java.util.List;

import huntermacdonald.com.calendar.R;
import huntermacdonald.com.calendar.activity.MainActivity;
import huntermacdonald.com.calendar.interfaces.RequestCallbackDb;
import huntermacdonald.com.calendar.model.DataCalendar;

/**
 * Created by quoctran on 27/10/2015.
 */
public class DatabaseHandler {

    private final String LOG_TAG = "DatabaseHandler";

    private CalendarDatabaseHelper mDb;

    public DatabaseHandler() {
        mDb = CalendarDatabaseHelper.getsInstance();
    }

    public void addListEvent(final List<DataCalendar> list, final String username, final RequestCallbackDb<Boolean> callbackDb) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (null != list && !TextUtils.isEmpty(username)) {
                    mDb.removeListEvent(username);
                    for (DataCalendar item : list) {
                        mDb.insertEvent(item);
                    }
                    updateUI(true, callbackDb);
                }
            }
        }).start();
    }

    public void addAccount(final String account, final int color, final RequestCallbackDb<Boolean> callbackDb) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (account != null && color != 0) {
                    Log.d(LOG_TAG,"[addAccount][color]"+color);
                    mDb.insertAccount(account, color);
                }
                updateUI(true, callbackDb);
            }
        }).start();
    }

    private <T> void updateUI(final T data, final RequestCallbackDb callback) {
        if (null != callback) {
            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(new Runnable() {
                @Override
                public void run() {
                    callback.onDbResult(data);
                }
            });
        }
    }

}
