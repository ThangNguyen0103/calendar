package huntermacdonald.com.calendar.utils;

/**
 * Created by quoctran on 23/11/2015.
 */
public class Statics {

    public static final int REQUEST_TIMEOUT_MS = 60000;

    //Outlook
    public static final String AUTHORITY_URL = "https://login.microsoftonline.com/common";
    public static final String DISCOVERY_RESOURCE_URL = "https://api.office.com/discovery/v1.0/me/";
    public static final String DISCOVERY_RESOURCE_ID = "https://api.office.com/discovery/";
    public static final String MAIL_CAPABILITY = "Calendar";
    public static final String CLIENT_ID = "9de38969-efa8-41a8-bfcf-cf3264fd2dd7";
    public static final String REDIRECT_URI = "http://www.huntermacdonald.co.uk/calendar";

    public static final String ACTION_NEW_EVENT = "com.huntermacdonald.calendar.new.event";

    public static final String SERVICE_ENDPOIN_URI = "https://outlook.office365.com/api/v1.0";
    public static final String RESOURCE_ID = "https://outlook.office365.com/";

    public static final String API_GET_EVENT_OUTLOOK = "https://outlook.office.com/api/v1.0/me/events";
    public static final String API_REFRESH_TOKEN_OUTLOOK = "https://login.microsoftonline.com/common/oauth2/token";
    public static final String BODY_REFRESH_TOKEN_OUTLOOK = "grant_type=refresh_token&refresh_token=<refresh_token>";


    //MainActivity
    public static final int REQUEST_ACCOUNT_PICKER = 1000;
    public static final int REQUEST_ACCOUNT_OUTLOOK = 1001;
    public static final int REQUEST_AUTHORIZATION = 1002;
    public static final int REQUEST_GOOGLE_PLAY_SERVICES = 1003;

    public static final int REQUEST_CODE_ASK_PERMISSIONS_ALL = 1023;
    public static final int REQUEST_CODE_ASK_PERMISSIONS_READ_CALENDAR = 1024;
    public static final int REQUEST_CODE_ASK_PERMISSIONS_READ_CONTAXT = 1025;

    public static final int COLOR_DEFAULT = 0xff0928a7;


}
