package huntermacdonald.com.calendar.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import java.security.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.StringTokenizer;
import java.util.TimeZone;

import huntermacdonald.com.calendar.CalendarApplication;

/**
 * Created by quoctran on 27/10/2015.
 */
public class Util {

    public static Calendar getTime(long time, TimeZone timeZone) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        calendar.setTimeZone(timeZone);

        return calendar;
    }

    public static long getStartMonth(int mMonth, int mYear) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(mYear, mMonth, 1, 00, 00, 00);
        Log.d("quoc-tran0", "[getStartMonth]" + calendar.getTime().toString());
        return calendar.getTimeInMillis();
    }

    public static long getEndMonth(int mMonth, int mYear) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(mYear, mMonth, 1, 23, 59, 59);
        calendar.add(Calendar.MONTH, 1);
        calendar.add(Calendar.DATE, -1);
        Log.d("quoc-tran0","[getEndMonth]"+calendar.getTime().toString());
        return calendar.getTimeInMillis();
    }

    public static boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) CalendarApplication.getInstance().getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        return (networkInfo != null && networkInfo.isConnectedOrConnecting());
    }

    public static String getFormatDatetime(Context mContext, Calendar date) {
        DateFormat df = android.text.format.DateFormat.getDateFormat(mContext);
        String dateTime = df.format(date.getTime());
        if (dateTime.startsWith(date.get(Calendar.YEAR) + "")) {

            dateTime = dateTime.replace(date.get(Calendar.YEAR) + "/", " ");

        } else {
            dateTime = dateTime.replace("/" + date.get(Calendar.YEAR), " ");

        }
        return " " + dateTime;
    }

    public static Calendar getCalendarFromDateTime(String dateTime) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        try {
            Date date = format.parse(dateTime);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            return calendar;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

}
