package huntermacdonald.com.calendar;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import huntermacdonald.com.calendar.interfaces.RequestCallback;
import huntermacdonald.com.calendar.model.DataCalendar;
import huntermacdonald.com.calendar.utils.Statics;
import huntermacdonald.com.calendar.utils.Util;

/**
 * Created by quoctran on 02/12/2015.
 */
public class WebServiceHandler {

    private final String LOG_TAG = "WebServiceHandler";

    private final String ACCESS_TOKEN_TAG ="access_token";
    private final String REFRESH_TOKEN_TAG ="refresh_token";
    private final String START_TIME_TAG ="Start";
    private final String END_TIME_TAG ="End";
    private final String SUBJECT_TAG ="Subject";
    private final String VALUE_TAG ="value";



    private SessionManager sessionManager;

    public WebServiceHandler(Context context) {
        sessionManager = new SessionManager(context);

    }

    public void getEventFromOffice365(final String usename, final RequestCallback<List<DataCalendar>> callback) {

        final String token = sessionManager.getAccessToken(usename);
        String url = Statics.API_GET_EVENT_OUTLOOK;
        JsonObjectRequest getEventRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(LOG_TAG, "[getEventFromOffice365][onResponse]" + response.toString());
                if (callback != null) {
                    try {
                        List<DataCalendar> list = new ArrayList<DataCalendar>();
                        JSONArray eventArray = response.getJSONArray(VALUE_TAG);
                        for (int i = 0; i < eventArray.length(); i++) {
                            JSONObject jsonObject = eventArray.getJSONObject(i);
                            DataCalendar temp = new DataCalendar();
                            temp.account = usename;
                            temp.startDate = Util.getCalendarFromDateTime(jsonObject.getString(START_TIME_TAG));
                            temp.endDate = Util.getCalendarFromDateTime(jsonObject.getString(END_TIME_TAG));
                            temp.title = TextUtils.isEmpty(jsonObject.getString(SUBJECT_TAG)) ? "no title" : jsonObject.getString(SUBJECT_TAG);
                            list.add(temp);
                        }
                        callback.onSuccess(list);
                    } catch (JSONException e) {
                        callback.onError();
                    }
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(LOG_TAG, "[getEventFromOffice365][onErrorResponse]" + error.getMessage());

                if (callback != null) {
                    callback.onError();
                }
            }

        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + token);
                return headers;
            }
        };

        CalendarApplication.getInstance().addToRequestQueue(getEventRequest, url);

    }

    public void refreshToken(final String usename, final RequestCallback<Boolean> callback) {

        final String body = Statics.BODY_REFRESH_TOKEN_OUTLOOK.replace("<refresh_token>",sessionManager.getRefreshToken(usename));
        final String url = Statics.API_REFRESH_TOKEN_OUTLOOK;

        final JsonObjectRequest putRefreshToken = new JsonObjectRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(LOG_TAG, "[onResponse]" + response.toString());
                try {
                    sessionManager.createNewToken(usename,response.getString(ACCESS_TOKEN_TAG),response.getString(REFRESH_TOKEN_TAG));
                    callback.onSuccess(true);
                } catch (JSONException e) {
                    e.printStackTrace();
                    callback.onError();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onError();
            }
        }) {
            @Override
            public byte[] getBody() {
                return body.getBytes();
            }

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded";
            }

            @Override
            protected String getParamsEncoding() {
                return "utf-8";
            }
        };
        CalendarApplication.getInstance().addToRequestQueue(putRefreshToken, url);

    }

}
