package huntermacdonald.com.calendar;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import huntermacdonald.com.calendar.utils.Statics;


/**
 * Created by quoctran on 20/11/2015.
 */
public class CatchChangeEventReceiver extends BroadcastReceiver {
    private final String LOG_TAG = "CatchChangeEventReceiver";
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(LOG_TAG,"[update event]");
        context.sendBroadcast(new Intent(Statics.ACTION_NEW_EVENT));
    }
}
