package huntermacdonald.com.calendar.fragment;

import android.graphics.RectF;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.alamkanak.weekview.DateTimeInterpreter;
import com.alamkanak.weekview.WeekView;
import com.alamkanak.weekview.WeekViewEvent;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import huntermacdonald.com.calendar.R;
import huntermacdonald.com.calendar.activity.MainActivity;
import huntermacdonald.com.calendar.utils.Util;

/**
 * Created by quoctran on 20/10/2015.
 */
public class DetailCalendarFragment extends Fragment implements WeekView.MonthChangeListener,
        WeekView.EventClickListener, WeekView.EventLongPressListener {

    private final String LOG_TAG = "DetailCalendarFragment";

    private static final String TAG_DATE = "tag_date";
    private static final String TAG_MONTH = "tag_month";
    private static final String TAG_YEAR = "tag_year";

    private int mDate, mMonth, mYear;

    private WeekView mWeekView;


    public static DetailCalendarFragment newInstance(int date, int month, int year) {
        DetailCalendarFragment mCalendarFragment = new DetailCalendarFragment();
        Bundle args = new Bundle();
        args.putInt(TAG_DATE, date);
        args.putInt(TAG_MONTH, month);
        args.putInt(TAG_YEAR, year);
        mCalendarFragment.setArguments(args);
        return mCalendarFragment;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail_calendar, container, false);

        mapView(view);

        initData();

        return view;
    }

    private void initData() {
        if (null != getArguments()) {
            mDate = getArguments().getInt(TAG_DATE);
            mMonth = getArguments().getInt(TAG_MONTH);
            mYear = getArguments().getInt(TAG_YEAR);


            // Show a toast message about the touched event.
            mWeekView.setOnEventClickListener(this);

            // The week view has infinite scrolling horizontally. We have to provide the events of a
            // month every time the month changes on the week view.
            mWeekView.setMonthChangeListener(this);

            // Set long press listener for events.
            mWeekView.setEventLongPressListener(this);

            // Set up a date time interpreter to interpret how the date and time will be formatted in
            // the week view. This is optional.
            setupDateTimeInterpreter(false);

            mWeekView.setNumberOfVisibleDays(1);

            Calendar calendar = Calendar.getInstance();

            calendar.set(mYear, mMonth, mDate);

            mWeekView.goToDate(calendar);


        }
    }

    private void mapView(View view) {
        mWeekView = (WeekView) view.findViewById(R.id.webkeview);

    }

    private void setupDateTimeInterpreter(final boolean shortDate) {
        mWeekView.setDateTimeInterpreter(new DateTimeInterpreter() {
            @Override
            public String interpretDate(Calendar date) {
                SimpleDateFormat weekdayNameFormat = new SimpleDateFormat("EEE", Locale.getDefault());
                String weekday = weekdayNameFormat.format(date.getTime());

                if (shortDate)
                    weekday = String.valueOf(weekday.charAt(0));
                return weekday.toUpperCase() + Util.getFormatDatetime(getActivity().getApplicationContext(), date);
            }

            @Override
            public String interpretTime(int hour) {
                if (DateFormat.is24HourFormat(getActivity().getApplicationContext())) {

                    return hour +"H";
                }
                return hour > 11 ? (hour - 12) + " PM" : (hour == 0 ? "12 AM" : hour + " AM");

            }
        });
    }

    @Override
    public void onEventClick(WeekViewEvent event, RectF eventRect) {

    }

    @Override
    public void onEventLongPress(WeekViewEvent event, RectF eventRect) {

    }

    @Override
    public List<WeekViewEvent> onMonthChange(int newYear, int newMonth) {
        List<WeekViewEvent> mListWeekViews = new ArrayList<WeekViewEvent>();
        mListWeekViews = ((MainActivity) getActivity()).checkEventInDate(newMonth, newYear);
        return mListWeekViews;
    }

    public void updateListEvent() {
        Log.d(LOG_TAG, "[onMonthChange]");
        mWeekView.notifyDatasetChanged();

    }
}
