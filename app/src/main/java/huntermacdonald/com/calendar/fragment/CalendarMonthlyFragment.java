package huntermacdonald.com.calendar.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Calendar;

import huntermacdonald.com.calendar.R;
import huntermacdonald.com.calendar.activity.MainActivity;
import huntermacdonald.com.calendar.adapter.CalendarFragmentAdapter;

/**
 * Created by quoctran on 22/10/2015.
 */
public class CalendarMonthlyFragment extends Fragment implements CalendarFragment.OnCalendarPagerChange, ViewPager.OnPageChangeListener {

    private final String LOG_TAG = "CalendarMonthlyFragment";

    public static final String[] Months = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};

    private ViewPager mViewPager;
    private CalendarFragmentAdapter mCalendarFragmentAdapter;
    private TextView txtMonth;
    private OnDateSelect mListener;

    public static CalendarMonthlyFragment newInstance() {
        CalendarMonthlyFragment mCalendarFragment = new CalendarMonthlyFragment();
        return mCalendarFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = (MainActivity) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_view_monthly, container, false);

        mapView(view);

        initData();

        return view;
    }

    private void mapView(View view) {
        txtMonth = (TextView) view.findViewById(R.id.txt_month);
        mViewPager = (ViewPager) view.findViewById(R.id.content_viewpager);
    }

    private void initData() {
        mCalendarFragmentAdapter = new CalendarFragmentAdapter(getActivity(), getChildFragmentManager());
        mViewPager.addOnPageChangeListener(this);
        mViewPager.setAdapter(mCalendarFragmentAdapter);
        mViewPager.setCurrentItem(1000);
        txtMonth.setText(getNameOfMonth0());
    }

    private String getNameOfMonth0() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, (mViewPager.getCurrentItem() - 1000));
        return Months[calendar.get(Calendar.MONTH)] + " - " + calendar.get(Calendar.YEAR);
    }

    @Override
    public void onCalendarSelectDate(int date, int month, int year) {
        if (null != mListener) {
            mListener.onSelectDateInCalendar(date, month, year);
        }
    }

    @Override
    public void onCloseDialogFromFragment() {
//        ((MainActivity) getActivity()).showDialog(false);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
    }

    @Override
    public void onPageScrollStateChanged(int state) {

        txtMonth.setText(getNameOfMonth0());
    }

    public void updateListEvent()
    {
        mCalendarFragmentAdapter.notifyDataSetChanged();
    }


    public interface OnDateSelect {
        void onSelectDateInCalendar(int date, int month, int year);
    }
}
