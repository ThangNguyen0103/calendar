package huntermacdonald.com.calendar.fragment;

import android.content.Context;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alamkanak.weekview.DateTimeInterpreter;
import com.alamkanak.weekview.WeekView;
import com.alamkanak.weekview.WeekViewEvent;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import huntermacdonald.com.calendar.R;
import huntermacdonald.com.calendar.activity.MainActivity;
import huntermacdonald.com.calendar.utils.Util;

/**
 * Created by quoctran on 22/10/2015.
 */
public class CalendarViewWeekly extends Fragment implements WeekView.MonthChangeListener,
        WeekView.EventClickListener, WeekView.EventLongPressListener {
    private final String LOG_TAG = "CalendarViewWeekly";

    private static final String TAG_DAY_VIEW = "tag_day_view";

    private WeekView mWeekView;
    private int numbDay;
    private OnDateSelectInWeekly mListener;


    public static CalendarViewWeekly newInstance(int numbDay) {
        CalendarViewWeekly mCalendarFragment = new CalendarViewWeekly();
        Bundle args = new Bundle();
        args.putInt(TAG_DAY_VIEW, numbDay);
        mCalendarFragment.setArguments(args);
        return mCalendarFragment;


    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = (MainActivity) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_view_weekly, container, false);

        mapView(view);

        initData();

        return view;
    }

    private void initData() {
        if (null != getArguments()) {

            numbDay = getArguments().getInt(TAG_DAY_VIEW);
            mWeekView.setOnEventClickListener(this);

            // The week view has infinite scrolling horizontally. We have to provide the events of a
            // month every time the month changes on the week view.
            mWeekView.setMonthChangeListener(this);

            // Set long press listener for events.
            mWeekView.setEventLongPressListener(this);

            // Set up a date time interpreter to interpret how the date and time will be formatted in
            // the week view. This is optional.
            setupDateTimeInterpreter(false);

            mWeekView.setNumberOfVisibleDays(numbDay);
            if (numbDay == 7) {
                setupDateTimeInterpreter(true);

                mWeekView.setColumnGap((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2, getResources().getDisplayMetrics()));
                mWeekView.setTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 10, getResources().getDisplayMetrics()));
                mWeekView.setEventTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 10, getResources().getDisplayMetrics()));
            }


        }
    }

    private void mapView(View view) {
        mWeekView = (WeekView) view.findViewById(R.id.weekview_weekly);
    }

    private void setupDateTimeInterpreter(final boolean shortDate) {
        mWeekView.setDateTimeInterpreter(new DateTimeInterpreter() {
            @Override
            public String interpretDate(Calendar date) {
                SimpleDateFormat weekdayNameFormat = new SimpleDateFormat("EEE", Locale.getDefault());
                String weekday = weekdayNameFormat.format(date.getTime());

                // All android api level do not have a standard way of getting the first letter of
                // the week day name. Hence we get the first char programmatically.
                // Details: http://stackoverflow.com/questions/16959502/get-one-letter-abbreviation-of-week-day-of-a-date-in-java#answer-16959657
                if (shortDate)
                    weekday = String.valueOf(weekday.charAt(0));
                return weekday.toUpperCase() + Util.getFormatDatetime(getActivity().getApplicationContext(),date);
            }

            @Override
            public String interpretTime(int hour) {
                if (DateFormat.is24HourFormat(getActivity().getApplicationContext())) {

                    return hour +"H";
                }
                return hour > 11 ? (hour - 12) + " PM" : (hour == 0 ? "12 AM" : hour + " AM");
            }
        });
    }

    @Override
    public void onEventClick(WeekViewEvent event, RectF eventRect) {
        if (mListener != null) {
            mListener.onSelectDateInWeekly(event.getStartTime());
        }
    }

    @Override
    public void onEventLongPress(WeekViewEvent event, RectF eventRect) {

    }

    @Override
    public List<WeekViewEvent> onMonthChange(int newYear, int newMonth) {
        List<WeekViewEvent> mListWeekViews = new ArrayList<WeekViewEvent>();
        mListWeekViews = ((MainActivity) getActivity()).checkEventInDate(newMonth,newYear);
        return mListWeekViews;
    }

    public void updateListEvent() {
        Log.d(LOG_TAG, "[onMonthChange]");
//        ((MainActivity) getActivity()).showDialog(false);
        mWeekView.notifyDatasetChanged();

    }


    public interface OnDateSelectInWeekly {
        void onSelectDateInWeekly(Calendar calendar);
    }


}
