package huntermacdonald.com.calendar.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import huntermacdonald.com.calendar.R;
import huntermacdonald.com.calendar.activity.MainActivity;
import huntermacdonald.com.calendar.adapter.CalendarAdapter;
import huntermacdonald.com.calendar.model.DataCalendar;
import huntermacdonald.com.calendar.model.DateCalendar;

/**
 * Created by quoctran on 19/10/2015.
 */

public class CalendarFragment extends Fragment implements CalendarAdapter.OnClickDateInCalendar {

    private final String LOG_TAG = "CalendarFragment";

    private static final String TAG_MONTH = "tag_month";
    private static final String TAG_YEAR = "tag_year";

    private GridView gridCalendar;
    private CalendarAdapter mCalendarAdapter;

    private int mMonth;
    private int mYear;

    private OnCalendarPagerChange mListener;

    private List<DataCalendar> listEvent;
    private List<DateCalendar> cells;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = (OnCalendarPagerChange) getParentFragment();
    }

    public static CalendarFragment newInstance(int month, int year) {
        CalendarFragment mCalendarFragment = new CalendarFragment();
        Bundle args = new Bundle();
        args.putInt(TAG_YEAR, year);
        args.putInt(TAG_MONTH, month);
        mCalendarFragment.setArguments(args);
        return mCalendarFragment;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_calendar, container, false);
        mapView(view);
        initData();
        return view;
    }

    private void initData() {

        if (null != getArguments()) {
            mMonth = getArguments().getInt(TAG_MONTH);
            mYear = getArguments().getInt(TAG_YEAR);

            listEvent = ((MainActivity) getActivity()).getListEventInMoth(mMonth, mYear);
            cells = new ArrayList<>();

            Calendar calendar = Calendar.getInstance();
            int currentMonth = mMonth - calendar.get(Calendar.MONTH);
            int currentYear = mYear - calendar.get(Calendar.YEAR);

            calendar.set(Calendar.MONTH, mMonth);
            calendar.set(Calendar.YEAR, mYear);


            calendar.set(Calendar.DAY_OF_MONTH, 1);
            int monthBegingCell = calendar.get(Calendar.DAY_OF_WEEK) - 1;

            calendar.add(Calendar.DAY_OF_MONTH, -monthBegingCell);
            if (currentMonth == 0 && currentYear == 0) {

                while (cells.size() < 42) {

                    DateCalendar temp = new DateCalendar();
                    temp.date = calendar.get(Calendar.DATE);
                    temp.titleObject = getEventIntoToday(temp.date);
                    cells.add(temp);


                    calendar.add(Calendar.DAY_OF_MONTH, 1);

                }
                mCalendarAdapter = new CalendarAdapter(getActivity(), this, cells, true, mMonth, mYear);
                gridCalendar.setAdapter(mCalendarAdapter);
            } else {

                while (cells.size() < 42) {

                    DateCalendar temp = new DateCalendar();
                    temp.date = calendar.get(Calendar.DATE);
                    temp.titleObject = getEventIntoToday(temp.date);
                    cells.add(temp);

                    calendar.add(Calendar.DAY_OF_MONTH, 1);

                }
                mCalendarAdapter = new CalendarAdapter(getActivity(), this, cells, false, mMonth, mYear);
                gridCalendar.setAdapter(mCalendarAdapter);
            }
        }
    }

    private List<DateCalendar.TitleCalendar> getEventIntoToday(int date) {
        List<DateCalendar.TitleCalendar> listEventTitle = new ArrayList<>();
        for (DataCalendar item : listEvent) {
            if (item.startDate.get(Calendar.DATE) == date) {
                DateCalendar.TitleCalendar titleObject = new DateCalendar.TitleCalendar();
                titleObject.title = item.title;
                titleObject.color = item.color;
                listEventTitle.add(titleObject);
            }
        }
        return listEventTitle;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(LOG_TAG, "[onResume]");
    }


    private void mapView(View view) {
        gridCalendar = (GridView) view.findViewById(R.id.calendar_grid);
    }

    @Override
    public void onClickDate(int date, int month, int year) {
//        Log.d("quoc-tran", "[onClickDate][calendar]" + calendar.getTime());
        mListener.onCalendarSelectDate(date, month, year);
    }

    @Override
    public void onCloseDialog() {
        if (null != mListener) {
            mListener.onCloseDialogFromFragment();
        }
    }

    public void updateData() {
        if (null != mCalendarAdapter) {
            Log.d(LOG_TAG, "[updateData]" + mMonth + mYear);
            if (((MainActivity) getActivity() != null)) {
                listEvent = ((MainActivity) getActivity()).getListEventInMoth(mMonth, mYear);
                updateDateList(listEvent);
                mCalendarAdapter.notifyDataSetChanged();
            }
        }
    }

    private void updateDateList(List<DataCalendar> list) {
        for (DateCalendar item : cells) {
            if (item.titleObject.size() > 0) {
                item.titleObject.clear();
            }
            for (DataCalendar event : list) {
                if (item.date == event.startDate.get(Calendar.DATE)) {

                    DateCalendar.TitleCalendar titleObject = new DateCalendar.TitleCalendar();
                    titleObject.title = event.title;
                    Log.d(LOG_TAG, "[updateDateList]" + event.color);
                    titleObject.color = event.color;
                    item.titleObject.add(titleObject);
                }
            }
        }
    }


    public interface OnCalendarPagerChange {
        void onCalendarSelectDate(int date, int month, int year);

        void onCloseDialogFromFragment();
    }
}

