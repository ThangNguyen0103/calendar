package huntermacdonald.com.calendar.model;

/**
 * Created by quoctran on 30/10/2015.
 */
public class AccountCalendar {

    public AccountCalendar(String name, boolean isCheck,boolean istype) {
        this.accountName = name;
        this.isCheck = isCheck;
        this.isType = istype;
    }

    public AccountCalendar() {
    }

    public String accountName;

    public boolean isCheck;

    public boolean isType;

    public boolean isSync;
}
