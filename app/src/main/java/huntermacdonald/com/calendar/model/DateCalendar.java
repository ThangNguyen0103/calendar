package huntermacdonald.com.calendar.model;

import java.util.List;

/**
 * Created by quoctran on 26/10/2015.
 */
public class DateCalendar {

    public int date;

    public List<TitleCalendar> titleObject;

    public static class TitleCalendar {

        public String title;

        public int color;
    }

}
