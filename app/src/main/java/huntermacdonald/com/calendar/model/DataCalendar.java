package huntermacdonald.com.calendar.model;

import com.google.gson.annotations.SerializedName;

import java.util.Calendar;

/**
 * Created by quoctran on 19/10/2015.
 */
public class DataCalendar {

    public Calendar startDate;

    public Calendar endDate;

    public String title;

    public int color;

    public String account;

}
